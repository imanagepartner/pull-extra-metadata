# 'dataset' holds the input data for this script
import json
import hashlib
import http.client
import os
import uuid
import csv
import urllib.request
for i in range(0, len(dataset)):
    id = dataset.loc[i,'Core ID']
    ##Server details
    Server = 'mvp1-insight'#EDIT THIS FIELD. ADD THE NAME OF YOUR SERVER.
    Port = 11040
    database = "ravn"
    ##Please change/add/remove the fields (from Core) within view.field to include the desired fields in the following uri
    ##For example, if the desired field is data.title the uri have to be changed for:
    ##uri = "http://" + Server + ":" + str(Port) + "/rest/api/v3.1/databases/" + database + "/items/" + id + "?rights.claims=superuser&view.field=data.title;
    uri = "http://" + Server + ":" + str(Port) + "/rest/api/v3.1/databases/" + database + "/items/" + id + "?rights.claims=superuser&view.field=data.work.library&view.field=data.work.docauthor&view.field=data.work.editwhen&view.field=data.work.KE.field14&view.field=data.work.KE.field09&view.field=data.work.KE.field03&view.field=data.work.KE.field04&view.field=data.work.KE.field05&view.field=data.work.KE.field10&view.field=data.work.KE.field01&view.field=data.work.KE.field11&view.field=data.work.KE.field06&view.field=data.work.KE.field12";
    try:
        f = urllib.request.urlopen(uri)
        res_json = json.loads(f.read().decode())
        ##The following fields are the fields to be presented in PowerBI (should be the ones included in the uri)
        ##For example, if we want to use the data.title field in PowerBI the dataset field used should be:
        ##dataset.loc[i,'Title'] = res_json['values']['data']['title']
        dataset.loc[i,'Library'] = res_json['values']['data']['work']['library']
        dataset.loc[i,'Author'] = res_json['values']['data']['work']['docauthor']
        dataset.loc[i,'Last Reviewed'] = res_json['values']['data']['work']['editwhen']
        dataset.loc[i,'Last Updated'] = res_json['values']['data']['work']['KE']['field14']
        dataset.loc[i,'Language'] = res_json['values']['data']['work']['KE']['field09']
        dataset.loc[i,'Document Type'] = res_json['values']['data']['work']['KE']['field03']
        dataset.loc[i,'Sub Document Type'] = res_json['values']['data']['work']['KE']['field04']
        dataset.loc[i,'Practice Group'] = res_json['values']['data']['work']['KE']['field05']
        dataset.loc[i,'Industry Sector'] = res_json['values']['data']['work']['KE']['field10']
        dataset.loc[i,'Knowledge Type'] = res_json['values']['data']['work']['KE']['field01']
        dataset.loc[i,'Matter Type'] = res_json['values']['data']['work']['KE']['field11']
        dataset.loc[i,'Jurisdiction'] = res_json['values']['data']['work']['KE']['field06']
        dataset.loc[i,'Governing Law'] = res_json['values']['data']['work']['KE']['field12']
    except:
        print('Not Found')